package pl.oskarskalski.core.vendor.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.oskarskalski.core.vendor.model.entity.Vendor;
import pl.oskarskalski.core.vendor.repository.VendorRepository;

import java.util.Date;
import java.util.UUID;

@Service
@AllArgsConstructor
public class VendorService {
    private VendorRepository vendorRepository;

    public void createTenant(String tenantName){
        UUID uuid = UUID.randomUUID();
        Vendor vendor = new Vendor();
        vendor.setVendorName(tenantName);
        vendor.setId(uuid.toString());
        vendor.setDate(new Date());
        vendor.setEnabled(true);
        vendorRepository.save(vendor);
    }
}
