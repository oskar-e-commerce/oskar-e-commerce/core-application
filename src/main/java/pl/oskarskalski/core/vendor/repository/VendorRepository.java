package pl.oskarskalski.core.vendor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.oskarskalski.core.vendor.model.entity.Vendor;

@Repository
public interface VendorRepository extends JpaRepository<Vendor, String> {
}
