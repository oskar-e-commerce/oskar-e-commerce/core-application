package pl.oskarskalski.core.vendor.product.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class Product {
    @Id
    @Column
    private int id;

    @Column
    private String vendorId;

    @Column
    private String productName;

    @Column
    private String description;

    @Column
    private Date date;

    @Column
    private String category;
}
