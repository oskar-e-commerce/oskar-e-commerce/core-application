CREATE TABLE vendor
(
    id          VARCHAR(50),
    vendor_name VARCHAR(100),
    is_enabled  VARCHAR(1),
    date        Date
);

CREATE TABLE product
(
    id           INT,
    vendor_id    VARCHAR(50),
    product_name VARCHAR(100),
    date         Date,
    description  VARCHAR(999),
    category     VARCHAR(20)
);

CREATE TABLE rating
(
    id        INT NOT NULL,
    user_id   VARCHAR(50),
    object_id VARCHAR(50),
    type      VARCHAR(1)
);

CREATE TABLE comment
(
    comment_id  INT NOT NULL,
    product_id  VARCHAR(50),
    user_id     VARCHAR(50),
    description VARCHAR(1)
);
